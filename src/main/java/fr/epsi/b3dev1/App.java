package fr.epsi.b3dev1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ResourceBundle;

/**
 * Hello world!
 *
 */
public class App 
{
    private static final Logger LOGGER = LoggerFactory.getLogger( App.class );
    
    public static void main( String[] args ) {
        ResourceBundle bundle = ResourceBundle.getBundle( "info" );
        
        LOGGER.info( bundle.getString( "app.name" ) + " -- Hello, world!" );
    }
}
